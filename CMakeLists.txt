cmake_minimum_required (VERSION 3.1)

project (clickhouse-client)

# let CMake figure out the correct flags by itself

set (CMAKE_CXX_STANDARD 11)
set (CMAKE_CXX_STANDARD_REQUIRED TRUE)

# in case you really want threads

set (CMAKE_THREAD_PREFER_PTHREAD TRUE)
set (THREADS_PREFER_PTHREAD_FLAG TRUE)
find_package (Threads REQUIRED)

add_subdirectory (libs/libbmp)
add_subdirectory (libs/libwgf)
add_subdirectory (libs/libwgf2)

SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fopenmp")

INCLUDE_DIRECTORIES(libs/libnetcdfc++/headers)

# Require MPI for this project:
find_package(MPI REQUIRED)
set(CMAKE_CXX_COMPILE_FLAGS ${CMAKE_CXX_COMPILE_FLAGS} ${MPI_COMPILE_FLAGS})
set(CMAKE_CXX_LINK_FLAGS ${CMAKE_CXX_LINK_FLAGS} ${MPI_LINK_FLAGS})
include_directories(MPI_INCLUDE_PATH)

find_package (NETCDF)

add_executable (main main.cpp)
target_link_libraries (
    main
        wgf
        wgf2
        bmp
        Threads::Threads
        ${MPI_LIBRARIES}
        #${CMAKE_CURRENT_SOURCE_DIR}/libs/libnetcdfc++/libnetcdf_c++4.a 
        #${CMAKE_CURRENT_SOURCE_DIR}/libs/libnetcdfc++/libnetcdf.a
        #${CMAKE_CURRENT_SOURCE_DIR}/libs/libnetcdfc++/libhdf5.a
)

