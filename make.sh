#!/bin/bash

#$a = "$(find . -type f \'\(.*cpp\|.*hpp\)\' -print0)"

cppFiles="$(find libs/ -type f -regex '.*cpp\|.*hpp' -print0 | xargs -0 echo) "

g++ -std=c++11 -o nc2wgf2 nc2wgf2.cpp $cppFiles -Ilibs/libbmp -Ilibs/libwgf -Ilibs/libwgf2 \
	/usr/lib/x86_64-linux-gnu/libnetcdf_c++.a \
	-L/usr/local/netcdf/lib -lnetcdf -I/usr/local/netcdf/include

g++ -std=c++11 -o nc2wgf3 nc2wgf3.cpp $cppFiles -Ilibs/libbmp -Ilibs/libwgf3 \
        /usr/lib/x86_64-linux-gnu/libnetcdf_c++.a \
        -L/usr/local/netcdf/lib -lnetcdf -I/usr/local/netcdf/include

g++ -std=c++11 -o wgf2draw wgf2draw.cpp $cppFiles -Ilibs/libbmp -Ilibs/libwgf2 \
        /usr/lib/x86_64-linux-gnu/libnetcdf_c++.a \
        -L/usr/local/netcdf/lib -lnetcdf -I/usr/local/netcdf/include

g++ -std=c++11 -o wgf3draw wgf3draw.cpp $cppFiles -Ilibs/libbmp -Ilibs/libwgf3 \
        /usr/lib/x86_64-linux-gnu/libnetcdf_c++.a \
        -L/usr/local/netcdf/lib -lnetcdf -I/usr/local/netcdf/include
