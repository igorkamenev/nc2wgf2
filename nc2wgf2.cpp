#include <strings.h>
#include <stdio.h>
#include <string.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>

#include <sstream>
#include <string>
#include <sys/stat.h>

#include "WGF2.hpp"
#include "netcdfcpp.h"

#include <string>
#include <sstream>
#include <vector>
#include <iterator>

using namespace  std;

vector<string> splitString(string str, char delim) {
    
    std::vector<std::string> elems;

    std::stringstream ss;
    ss.str(str);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    
    return elems;
}


class Variable {
    
public:
    
    NcVar* var;
    long width;
    long height;
    int additionalDimensionIndex;
    string outputFileName;
    
};

void interpolate(char* outputDir,
                 vector<Variable> variables,
                 NcVar* latVar,
                 NcVar* lonVar,
                 WGF2Header config
                 )
{
    
    mkdir(outputDir, 0000777);

    for (int idx=0; idx < variables.size(); idx++) {
        
        Variable var = variables[idx];
        cout << "Processing " << var.var->name() << "\n";
        
        float emptyValue = __FLT_MAX__;
        for (int i=0; i < var.var->num_atts(); i++) {
            NcAtt* att = var.var->get_att(i);
            if (string(att->name()) == "_FillValue") {
                NcValues* v = att->values();
                emptyValue = v->as_float(0);
                break;
            }
        }

        config.emptyValue = emptyValue;
        
        char outputFilePath[1024];
        sprintf(outputFilePath, "%s/%s.wgf2", outputDir, var.outputFileName.c_str());
        printf("Output to file: %s\n", outputFilePath);
        
        WGF2 wgf;
        wgf.open(outputFilePath, config);
        
        NcValues* values = var.var->values();
        NcValues* latValues = latVar->values();
        NcValues* lonValues = lonVar->values();
        
        long valuesCount = latVar->num_vals();
        
        for (int valIdx=0; valIdx < valuesCount; valIdx++) {
            
            float lon = lonValues->as_float(valIdx);
            float lat = latValues->as_float(valIdx);

            float val = values->as_float(valIdx + valuesCount*var.additionalDimensionIndex);

            if (val == config.emptyValue) {
                continue;
            }
            
            wgf.writeAvg(lat, lon, val);
            wgf.writeAvg(lat-config.dLat, lon, val);
            wgf.writeAvg(lat+config.dLat, lon, val);

            wgf.writeAvg(lat, lon-config.dLon, val);
            wgf.writeAvg(lat-config.dLat, lon-config.dLon, val);
            wgf.writeAvg(lat+config.dLat, lon-config.dLon, val);
            
            wgf.writeAvg(lat, lon+config.dLon, val);
            wgf.writeAvg(lat-config.dLat, lon+config.dLon, val);
            wgf.writeAvg(lat+config.dLat, lon+config.dLon, val);
        }
        
        printf("done\n");
        
        char outputImagePath[1024];
//        sprintf(outputImagePath, "%s/%s.bmp", outputDir, var.outputFileName.c_str());
//        wgf.drawToBitmap(outputImagePath, 15.0);
        wgf.close();
    }
}

void show_netcdf_file(char* inFile) {
    
    NcFile dataFile(inFile, NcFile::ReadOnly);
 
    cout <<"Dimension:\n";
    for (int i=0; i < dataFile.num_dims(); i++) {
        
        NcDim* dim = dataFile.get_dim(i);
        cout << "\t" << dim->name() << " size: " << dim->size() << "\n";
    }
    
    cout <<"Variables:\n";
    for (int i=0; i < dataFile.num_vars(); i++) {
        
        NcVar* var = dataFile.get_var(i);
        
        cout << "\t" << var->name() << " [";
        
        for (int j=0; j < var->num_dims(); j++) {
            NcDim* dim = var->get_dim(j);
            cout << dim->name();
            if (j+1 < var->num_dims()) {
                cout << ",";
            }
        }
        
        cout << "]\n";
    }
    
}


int main(int argc,char **argv)
{
    
    if (argc == 2) {
        
        show_netcdf_file(argv[1]);
        
        return 0;
    }

    if (argc < 13) {
        printf("Usage: netcdf2csv <input netcdf file> [<output directory> <xParamName> <yParamName> <latParamName> <lonParamName> <latMin> <latMax> <lonMin> <lonMax> <dlat> <dlon> <param 1> <param 2> ... <param N>]\n");
        return 0;
    }
    
    char* inFile = argv[1];
    char* outputDir = argv[2];
    char* xParamName = argv[3];
    char* yParamName = argv[4];
    char* latParamName = argv[5];
    char* lonParamName = argv[6];
    
    float latMin = atof(argv[7]);
    float latMax = atof(argv[8]);
    float lonMin = atof(argv[9]);
    float lonMax = atof(argv[10]);
    float dLat = atof(argv[11]);
    float dLon = atof(argv[12]);
    
    int firstParamIndexInArgs = 13;
    int paramsCount = argc - firstParamIndexInArgs;
    
    WGF2Header config;
    config.latMin = latMin;
    config.latMax = latMax;
    config.lonMin = lonMin;
    config.lonMax = lonMax;
    config.dLat = dLat;
    config.dLon = dLon;
    config.emptyValue = __FLT_MAX__;


    NcFile dataFile(inFile, NcFile::ReadOnly);
    
    cout << "Checking x dimension with name '" << xParamName << "' ...";
    NcDim* xDim = dataFile.get_dim(xParamName);
    cout << "ok\n";
    
    cout << "Checking y dimension...";
    NcDim* yDim = dataFile.get_dim(yParamName);
    cout << "ok\n";

    cout << "Input file is " << xDim->size() << "x" << yDim->size() << "\n";
    
    cout << "Checking latitude variable...";
    NcVar* latVar = dataFile.get_var(latParamName);
    cout << "ok\n";
    
    cout << "Checking longitude variable...";
    NcVar* lonVar = dataFile.get_var(lonParamName);
    cout << "ok\n";
    
    vector<Variable> variables;
    
    for (int paramIdx=0; paramIdx < paramsCount; paramIdx++) {
        
        char* paramName = argv[firstParamIndexInArgs+paramIdx];

        string variableName;
        string additionalDimensionName;
        string additionalDimensionValue;
        string outputFileName;
        
        string fullVariableName = string(paramName);
        int additionalDimensionIdx = 0;
        
        vector<string> variableParts = splitString(fullVariableName, ':');

        bool isAdditionalDimensionExists = variableParts.size() > 2;
        
        cout << variableParts.size() << "\n";
        
        if ((isAdditionalDimensionExists && variableParts.size() < 4) || variableParts.size() < 2) {
            cout << "Malformed param name '" << fullVariableName << "' => should be param:outputFileName:outputFileName[:additionalDimension:value]\n";
            return 1;
        }
        
        variableName = variableParts[0];
        outputFileName = variableParts[1];

        if (isAdditionalDimensionExists) {
            additionalDimensionName = variableParts[2];
            additionalDimensionValue = variableParts[3];
        }
        
        cout << "Checking params " << variableName << "... ";
        NcVar* var = dataFile.get_var(variableName.c_str());
        
        cout << "Ok.\n";
        
        if (isAdditionalDimensionExists) {
            cout << "Additional dimension is '" << additionalDimensionName << "'=" << additionalDimensionValue << "\n";
        }
        
        Variable outputVariable;
        outputVariable.width = xDim->size();
        outputVariable.height = yDim->size();
        outputVariable.var = var;
        outputVariable.additionalDimensionIndex = 0;
        outputVariable.outputFileName = outputFileName;
        
        if (isAdditionalDimensionExists) {

            cout << "Checking dimensions...\n";
            
            for (int dimensionNumber=0; dimensionNumber < var->num_dims(); dimensionNumber++) {
                
                NcDim* dim = var->get_dim(dimensionNumber);

                if (dim->name() != additionalDimensionName) {
                    continue;
                }
                
                cout << "Dimension #" << dimensionNumber << " " << dim->name() << "\n";
                
                NcVar* dimVar = dataFile.get_var(dim->name());
                NcValues* dimValues = dimVar->values();
                
                bool found = false;
                for (int valNum=0; valNum < dimVar->num_vals(); valNum++) {
                    
                    char dimValStr[1024];
                    sprintf(dimValStr, "%g", dimValues->as_double(valNum));
                    string dimVal = string(dimValStr);
                    cout << valNum << ": " << dimValues->as_double(valNum) << "...";
                    if (dimVal == additionalDimensionValue) {
                        cout << "Ok\n";
                        found = true;
                        additionalDimensionIdx = valNum;
                        break;
                    } else {
                        cout << "\n";
                    }
                }
                
                if (!found) {
                    cout << "Value " << additionalDimensionValue << " not found in " << dimVar->name() << "\n";
                    return 1;
                }
            }

            outputVariable.additionalDimensionIndex = additionalDimensionIdx;
        }
        
        variables.push_back(outputVariable);
    }
    
    cout << "Variables to process: " << variables.size() << "\n";
    
    interpolate(outputDir, variables, latVar, lonVar, config);
    
    return 0;
}





