//
//  wgf2draw.cpp
//  NC2WGF2
//
//  Created by Igor Kamenev on 07/04/2017.
//  Copyright © 2017 Igor Kamenev. All rights reserved.
//

#include <strings.h>
#include <stdio.h>
#include <string.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>

#include <sstream>
#include <string>
#include <sys/stat.h>

#include "wgf3.h"
#include "netcdfcpp.h"

#include <string>
#include <sstream>
#include <vector>
#include <iterator>

using namespace  std;
int main(int argc,char **argv)
{
    
    if (argc < 2) {
        printf("Usage: wgf2draw <input wgf2 file> <output image>\n");
        return 0;
    }
    
    char* inFile = argv[1];
    char* outputDir = argv[2];
    
    WGF3 wgf;
    if (!wgf.open(inFile)) {
        cout << "Wrong wgf3 file name\n";
        return 1;
    }
    
    wgf.drawToBitmap(outputDir, 30);
    wgf.close();
    
}

